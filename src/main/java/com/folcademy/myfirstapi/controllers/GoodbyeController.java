package com.folcademy.myfirstapi.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodbyeController {
    @RequestMapping("/Goodbye")
    public String Goodbye(){
        return "Goodbye World!";
    }
}
